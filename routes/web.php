<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});
Route::get('/unoauno', function () {
  return view('unoauno');
})->name('unoauno');

Route::get('/editor', function () {
  return view('editor');
})->name('editor');

Route::get('/admin', function () {
  return view('admin');
})->name('admin');

// Route::get('social/google', function () {
//   return "social";
// })->name('social');

Route::get('social/{provider?}', 'SocialController@getSocialAuth')->name('social');
Route::get('social/callback/{provider?}', 'SocialController@getSocialAuthCallback');

Route::get('contenido/{id}', 'EstudioController@mostrar');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('estudios', 'EstudioController');

Route::resource('temas', 'TemaController');

Route::resource('paginas', 'PaginaController');

Route::resource('imagens', 'imagenController');

Route::resource('users', 'UserController');

// Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::get('auth/confirm/email/{email}/confirm_token/{confirm_token}', 'Auth\AuthController@confirmRegister');
Route::post('auth/register', 'Auth\AuthController@create');


Route::get('sendemail', function () {

    $data = array(
        'name' => "Curso Laravel",
    );

    Mail::send('emails.welcome', $data, function ($message) {

        $message->from('abrahamcodero@gmail.com', 'Curso Laravel');

        $message->to('abrahamicm2@gmail.com')->subject('test email Curso Laravel');

    });

    return "Tú email ha sido enviado correctamente";

});
