<!DOCTYPE html>
<html lang="en">
<head>
  <title>Estudios Biblicos</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1" value="abraham@hotmail.com">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="{{str_replace('server.php','',asset('css/css.css'))}}">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <!--  <script src="https://unpkg.com/vue"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->

</head>
<body> 
 
@if ($errors->any())
   <div class="alert alert-danger alert-dismissable fade in" style="position: fixed;right: 0">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


       
 <div class="modal fade" id="ingresar">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         <h4 class="modal-title text-center">Ingresar</h4>
       </div>
       <div class="modal-body">
        <form class="form-horizontal"  method="POST" action="{{ url('login') }}">
          
             {{ csrf_field() }}
            <div class="form-group">
               <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="email">Email:</label>
               <div class="col-sm-8 ">
                 <input type="email" class="form-control input-sm" id="email-ingresar" placeholder="Introduce email" name="email" value="abraham@hotmail.com">
               </div>
             </div>
             <div class="form-group">
                <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="password">Password:</label>
                <div class="col-sm-8 ">
                  <input type="password" class="form-control input-sm" id="password" placeholder="Introduce password" name="password" value="abraham@hotmail.com">
                </div>
              </div>
            
      
         
       </div>
       <div class="modal-footer">
         <button type="submit" class="btn btn-default" >Ingresar</button>
         
       </div>
         </form>
     </div>
   </div>
 </div>

    <div class="modal" id="registracion">
      <div >
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
           <h4 class="modal-title text-center">Registracion</h4>
         </div>
         <div class="modal-body">
<form class="form-horizontal" method="POST"  action="{{url('auth/register') }}">
 {{ csrf_field() }}
 <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="name">Nombre:</label>
    <div class="col-sm-8 ">
      <input type="text" class="form-control input-sm" id="nombre" placeholder="Introduce nombre" name="name" value="abraham">
    </div>
  </div>

  <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="apellido">Apellido:</label>
    <div class="col-sm-8 ">
      <input type="text" class="form-control input-sm" id="apellido" placeholder="Introduce apellido" name="apellido" value="cordero">
    </div>
  </div>

 <!--  <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="avatar">Avatar:</label>
    <div class="col-sm-8 ">
      <input type="file"  id="avatar" placeholder="Introduce avatar" name="avatar" value="abraham@hotmail.com">
    </div>
  </div>
 -->
  <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="cumpleaños">Cumpleaños:</label>
    <div class="col-sm-8 ">
      <input type="date" class="form-control input-sm" id="cumpleaños" placeholder="Introduce cumpleaños" name="cumpleanos" style="color: #2b78c3;" required">
    </div>
  </div>

  

   <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="iglesia">Iglesia:</label>
    <div class="col-sm-8 ">
        <select id="iglesia" class="form-control input-sm" name="iglesia"   value="Tegucigalpa HN">
             <option>Tegucigalpa HN</option>
             <option>San Pedro Sula HN</option>
             <option>Choluteca HN</option>
             <option>La ceiba HN</option>
             <option>Charlotte USA</option>


          </select>
    </div>
  </div>

  <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="contraseña">Contraseña:</label>
    <div class="col-sm-8 ">

      <input type="password" class="form-control input-sm" id="contraseña" placeholder="Introduce contraseña" name="password" value="abraham@hotmail.com">
    </div>
  </div>
  <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="contraseña">Confirmar Contraseña:</label>
    <div class="col-sm-8 ">

      <input type="password" class="form-control input-sm" id="confir_contraseña" placeholder="Confirmar Contraseña" name="password_confirmation" value="abraham@hotmail.com">
    </div>
  </div>

  <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="email">Email:</label>
    <div class="col-sm-8 ">
      <input type="text" class="form-control input-sm" id="email-registracion" placeholder="Introduce email" name="email" value="abraham@hotmail.com">
    </div>
  </div>

  <div class="form-group">
    <label style="font-weight: regular !important;" class=" col-sm-4 text-left" for="telefono">Telefono:</label>
    <div class="col-sm-8 ">
      <input type="text" class="form-control input-sm" id="telefono" placeholder="Introduce telefono" name="telefono" value="04124525546">
    </div>
  </div>



           
         </div>
         <div class="modal-footer">
          
           <button type="submit" class="btn btn-default">Crear Cuenta</button>
         </div>
</form>
       </div>
     </div>
   </div>
   </div> 



   <!--  <div class="cabecera"></div> -->
   <div>
     
   <img src="{{str_replace("server.php","",asset('img/fondo1.png'))}}" style="height: 100vh;position: absolute; z-index: -2;width: 100%;">
    
   </div>
  <div class="container">
      <div class="logo"> <h3 style="color: white">SEMBRAR</h3></div>
    <div class="flexible">
    
      <div class="mt40"  ><img src="{{str_replace("server.php","",asset('img/logo.png'))}}" style="margin: 0 auto; display: block; width: 100%" ></div>
      <!-- <div style="color: white;font-size: 40px;text-align: center;">IMPACTO</div>
      <div style="color: white;text-align: center;">la palabra de Dios aplicada al diario vivir</div> -->
      <div class="mt10">
        <a href='{{route('social', ['provider' => 'facebook'])}}' ><img src="{{str_replace("server.php","",asset('img/faccebook-icon.png'))}}"></a> 
        <a href='{{url("social/google")}}'><img src="{{str_replace("server.php","",asset('img/gmail-icon.png'))}}"></a>
      </div>
      <div class="mt10">
        <button class="btn btn-primary" data-toggle="modal" data-target='#ingresar'>Ingresar</button>
        <button class="btn btn-primary" data-toggle="modal" data-target='#registracion'>Registracion</button>
      </div>
     
    
      
    </div>
    
    

  </div>

  <div class="container">
    <div class="row" style="color:white">
      <div class="col-sm-4 pull-right">
        <h3>Proverbios 2 : 1-5 </h3>

        1. Hijo mío, si recibieres mis palabras, Y mis mandamientos guardares dentro de ti,
        2. Haciendo estar atento tu oído a la sabiduría; Si inclinares tu corazón a la prudencia,
        3. Si clamares a la inteligencia, Y a la prudencia dieres tu voz;
        4. Si como a la plata la buscares, Y la escudriñares como a tesoros,
        5.Entonces entenderás el temor de Jehová, Y hallarás el conocimiento de Dios.
        
      </div>


    </div>
    
  </div>
   
   <br>
  
<footer class="pie color-blanco">
     2018 © Copyright., IMPACTO
    </footer>
</body>
</html>