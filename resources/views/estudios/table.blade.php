<table class="table table-responsive" id="estudios-table">
    <thead>
        <th>Nombre</th>
        <th>Categoria</th>
        <th>Descripcion</th>
        <th>Imagen</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($estudios as $estudio)
        <tr>
            <td>{!! $estudio->nombre !!}</td>
            <td>{!! $estudio->categoria !!}</td>
            <td>{!! $estudio->descripcion !!}</td>
            <td><img src="/~impactoresources/storage/app/{{$estudio->imagen}}"  width="75" style="max-height: 150px"></td>
     
            <td>
                {!! Form::open(['route' => ['estudios.destroy', $estudio->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('estudios.show', [$estudio->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('estudios.edit', [$estudio->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>