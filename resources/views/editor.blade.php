
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
<title>Learning</title>
<link rel="stylesheet" type="text/css" href="{{str_replace( 'server.php' ,'',  asset('css/uikit.min.css')) }}">
<link rel="stylesheet" type="text/css" href="{{str_replace( 'server.php' ,'',  asset('css2/css.css')) }}">
<link rel="stylesheet" type="text/css" href="{{str_replace( 'server.php' ,'',  asset('css/boot.css')) }}">
<link rel="stylesheet" type="text/css" href="{{str_replace( 'server.php' ,'',  asset('css/book.css')) }}">
<style type="text/css">
  p {word-wrap: break-word;}
  a {line-height: 2.4}
  
  .contenedor{ margin: 20px;	 }
</style>
<style type="text/css">
a.editor{
  text-decoration: none;
  color: #FFF;
  width: 30px;
  height: 30px;
  display: inline-block;
  text-align: center;
  margin: 0;
  background-color: #000;
}
ul.editor{
  list-style: none;
  margin: 0;
  padding: 0;
}

/*textarea {display: none;}*/
/*input {letter-spacing: -99px;border: none;}*/
a.editor:hover{
  padding: 0 20px;text-align: left;
  background: #000;
}

/*li:hover input , textarea{
  letter-spacing: normal;
  border: 1px solid;
  display: inline;
}*/

.uk-icon>svg:nth-child(2) {display: none;}
</style>

</head>
<body>

	<div  id ="app" uk-grid>
	  <!-- modal titulo-->
	  <div id="modal-titulo" uk-modal>
	      <div class="uk-modal-dialog uk-modal-body">
	          <button class="uk-modal-close-default" type="button" uk-close></button>
	          <h2 class="uk-modal-title">Ingrese el titulo</h2>
	          <p>
	            <input type="text" v-model="titulos" class="uk-input"  @keyup="keyup_titulo()" @blur="resetear_valores()" @keyup.enter="enter_titulo()" > 

	          </p>
	      </div>
	  </div>
	   <!-- modal versiculi-->
	  <div id="modal-versiculo" uk-modal>
	      <div class="uk-modal-dialog uk-modal-body">
	          <button class="uk-modal-close-default" type="button" uk-close></button>
	          <h2 class="uk-modal-title">Ingrese el versiculo</h2>
	          <p>
	           <input type="text" v-model="versiculos" class="uk-input" @keyup="keyup_versiculo()" @blur="resetear_valores()" @keyup.enter="enter_versiculo()" >

	          </p>
	      </div>
	  </div>
	     <!-- modal versiculo-->
	  <div id="modal-respuesta" uk-modal>
	      <div class="uk-modal-dialog uk-modal-body">
	          <button class="uk-modal-close-default" type="button" uk-close></button>
	          <h2 class="uk-modal-title">Ingrese el versiculo</h2>
	          <p>
	           <input type="text" v-model="respuestas" class="uk-input" @keyup="keyup_respuesta()"  @blur="resetear_valores()"  >
	          </p>
	      </div>
	  </div>
	       <!-- modal versiculo-->
	  <div id="modal-negrita" uk-modal>
	      <div class="uk-modal-dialog uk-modal-body">
	          <button class="uk-modal-close-default" type="button" uk-close></button>
	          <h2 class="uk-modal-title">Ingrese el texto</h2>
	          <p>
	           <input type="text" v-model="negritas" class="uk-input"   @keyup="keyup_negrita()"  @blur="resetear_valores()" @keyup.enter="enter_negrita" >
	          </p>
	      </div>
	  </div>
	         <!-- modal versiculo-->
	  <div id="modal-parrafo" uk-modal>
	      <div class="uk-modal-dialog uk-modal-body">
	          <button class="uk-modal-close-default" type="button" uk-close></button>
	          <h2 class="uk-modal-title">Ingrese parrafo</h2>
	          <p>
	         <textarea v-model="parrafos" class="uk-textarea" @keyup="keyup_parrafo()" @blur="resetear_valores()" @keyup.enter="enter_parrafo"></textarea>
	          </p>
	      </div>
	  </div>
	  
	<div  style="position: absolute;top:100px;z-index:200">
	  <ul class="editor" >
	  <!--   <li> <a @click="agrega_pagina()" class="editor" ><span class="fa fa-plus "> </span></a></li>
	    <li> <a  @click="set_id_izquierda()" class="editor" ><span class="fa fa-chevron-left"> </span></a></li>
	    <li> <a  @click="set_id_derecha()" class="editor" ><span class="fa fa-chevron-right"> </span></a></li> -->
	    <li> <a class="editor" href="#modal-titulo" uk-toggle><span class="fa fa-font "> </span></a></li>
	    <li> <a class="editor" href="#modal-versiculo" uk-toggle><span class="fa fa-book"> </span></a></li>
	    <li> <a class="editor" @click="enter_respuesta()" ><span class="fa fa-edit"> </span></a></li>
	    <li> <a class="editor" href="#modal-negrita" uk-toggle><span class="fa fa-bold"> </span></a> </li>
	    <li> <a class="editor" href="#modal-parrafo" uk-toggle><span class="fa fa-align-justify "> </span></a></li>
	    <li>
	      <a class="editor" @click.prevent="borrar_contenido()"><span  class="fa  fa-trash" @click="borrar_contenido"></span></a>
	    </li>
	    <li>
	      <a class="editor" @click.prevent="borrar_contenido()"><span  class="fa  fa-upload"></span></a>
	    </li>
	  </ul>
	  <pre>@{{$data}}</pre>
	</div>
	<div class="uk-width-1-1">
	  
	  <div  class="bg-inverse uk-light" uk-grid>
	    <div class="uk-width-1-2">  
	      <a href="#" class="uk-margin-left"><span class="uk-margin-small-right" uk-icon="icon: list; ratio: 1"></span></a>
	      <a href="#"><span class="uk-margin-small-right" uk-icon="icon: tag; ratio: 1"></span></a>
	      <a href="#"><span class="uk-margin-small-right" uk-icon="icon: search; ratio: 1"></span></a>  
	    </div>
	    <div class="uk-width-1-2 uk-text-right">
	      <a href="#"><span class="uk-margin-small-right" uk-icon="icon:  pencil; ratio: 1"></span></a>     
	      <a href="#"><span class="uk-margin-small-right" uk-icon="icon:  play; ratio: 1"></span></a>   
	      <a href="#" class="uk-margin-right"><span class="uk-margin-small-right" uk-icon="icon: bookmark; ratio: 1"></span></a>  
	    </div>
	  </div>

	  <!-- este es el slider -->
	  <div>
	    <div class="uk-position-relative" uk-slideshow >
	     <ul class="uk-slideshow-items "   uk-height-viewport="expand: true;min-height: 300">
	     	<li style="overflow: auto;padding: 0 30px" id="page_0"><div class="uk-margin" uk-grid><div class="uk-width-1-1@m"  ><div class="contenedor"><div class="izquierda">
<div id="contenido"> <div >
	<h3 class="uk-text-center">haga click en la letra para agregar contenido</h3>
<p>click en libro para agregar versiculo</p>
<p>click en  lapiz para insertar pregntapara</p>
<p>click en  la b para agregar negrita</p>
<p>click en las barras para agregar contenido</p>
<p>click en  el bote de basura para eliminar tetxo</p>
<p>click en  el la flecha para guardar</p>

</div></div>
	     	</div></div></div></li>


	     </ul>
	      <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
	      <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
	    </div>
	  </div>

<!-- 	  <div class="fixed-bottom uk-text-center">
	    <div class="uk-margin uk-width-5-6 uk-align-center">
	      <input class="uk-range" type="range" value="2" min="0" max="10" step="0.1" >
	    </div>
	    <div  class="bg-inverse uk-light" uk-grid>
	      <div class="uk-width-1-3">Titulo del estudio</div>
	      <div class="uk-width-1-3">1%</div>
	      <div class="uk-width-1-3">Capitulo</div>
	    </div>
	  </div> -->
	</div>
	</div>

 


<script src=" {{str_replace( 'server.php' ,'',  asset('vue.js'))}}"></script>
<script src=" {{str_replace( 'server.php' ,'',  asset('axios.js'))}}"></script>
<script src=" {{str_replace( 'server.php' ,'',  asset('js/uikit.js'))}}"></script>
<script src=" {{str_replace( 'server.php' ,'',  asset('js/uikit-icons.js'))}} "></script>
<script src=" {{str_replace( 'server.php' ,'',  asset('jquery-3.3.1.min.js'))}} "></script>
<script  src=" {{str_replace( 'server.php' ,'',  asset('fontawesome-all.min.js'))}}"></script>
<script >
//UIkit.use(Icons);
$(document).ready(function() {
	var contandor =0;
var app = new Vue({
el: '#app',
data: {
  pagina: "",
  titulos: "",
  versiculos: "",
  preguntas: "",
  respuestas: "",
  negritas: "",
  parrafos: "",
  contenido_temporal:"",
  contenido_guardado:""
},
mounted: function() {
  console.log("se monto el elemento de vue");
  var contenido = document.getElementById('contenido');
  //var contenido_izquierda = document.querySelectorAll('.contenido_izquierda');
  //var contenido_derecha = document.querySelectorAll('.contenido_derecha');
  


},
methods: {
  keyup_parrafo: function () {
    this.contenido_temporal= '<p class="s7">'+this.parrafos+ '</p>';
    contenido.innerHTML=this.contenido_guardado+this.contenido_temporal;

  },
   keyup_titulo: function () {
    this.contenido_temporal="<h3 class='s6'>" + this.titulos + "</h3>";
    contenido.innerHTML=this.contenido_guardado+this.contenido_temporal;

  },
   keyup_versiculo: function () {
    this.contenido_temporal="<p class='s10'>" + this.versiculos + "</p>";
    contenido.innerHTML=this.contenido_guardado+this.contenido_temporal;
  },
   keyup_pregunta: function () {
    this.contenido_temporal= this.preguntas + "<input>";
    contenido.innerHTML=this.contenido_guardado+this.contenido_temporal;
  },
   keyup_respuesta: function () {
    this.contenido_temporal= "<input>";
    contenido.innerHTML=this.contenido_guardado+this.contenido_temporal;
  },
   keyup_negrita: function () {
    this.contenido_temporal= "<p class='s8 s9'>" + this.negritas + "</p>";
    contenido.innerHTML=this.contenido_guardado+this.contenido_temporal;
  },
  enter_parrafo: function () {
    this.contenido_guardado+= '<p class="s7">'+this.parrafos+ '</p>';
    this.contenido_temporal="";
    this.parrafos="";
    UIkit.modal("#modal-parrafo").hide();
      UIkit.notification({message: 'Parrafo guardado',timeout: 500});
  },
   enter_titulo: function () {
    this.contenido_guardado +="<h3 class='s6'>" + this.titulos + "</h3>";
    this.contenido_temporal="";
    this.titulos="";
    UIkit.modal("#modal-titulo").hide();
    UIkit.notification({message: 'Titulo guardado',timeout: 500});
    
  },
   enter_versiculo: function () {
    this.contenido_guardado+="<p class='s10'>" + this.versiculos + "</p>";
    this.contenido_temporal="";
    this.versiculos="";
    UIkit.modal("#modal-versiculo").hide();
    UIkit.notification({message: 'Versiculo guardado',timeout: 500});
  },
   enter_pregunta: function () {
    this.contenido_guardado+= this.preguntas;
    this.contenido_temporal="";
    this.preguntas="";
    UIkit.modal("#modal-pregunta").hide();
    UIkit.notification({message: 'Pregunta guadada',timeout: 500});
  },
   enter_respuesta: function () {
   	this.contenido_temporal= "<input>";
   	contenido.innerHTML=this.contenido_guardado+this.contenido_temporal;
    this.contenido_guardado+= " <input> ";
    this.contenido_temporal="";
    
    // UIkit.modal("#modal-respuesta").hide();
    UIkit.notification({message: 'Pregunta guadada',timeout: 500});
  },
   enter_negrita: function () {
    this.contenido_guardado+="<p class='s8 s9'>" + this.negritas + "</p>";
    this.contenido_temporal="";
    this.negritas="";
    UIkit.modal("#modal-negrita").hide();
    UIkit.notification({message: 'Palabra guardada',timeout: 500});
  },
  resetear_valores: function(){
    this.pagina = "";
    this.titulos = "";
    this.versiculos = "";
    this.preguntas = "";
    this.respuestas = "";
    this.negritas = "";
    this.parrafos = "";
    this.contenido_temporal = "";
    contenido.innerHTML=this.contenido_guardado+this.contenido_temporal;
    console.log("reseteando");
  },
  agrega_pagina:function(){

  	//UIkit.slideshow(".uk-slideshow").index;
  	UIkit.notification({message: 'Pagina agreagada',timeout: 500});
  	$('.uk-slideshow-items').append('<li style="overflow: auto;" id="page_0"><div class="uk-margin" uk-grid><div class="uk-width-1-2@m"  ><div class="contenedor"><div class="izquierda">izquierda</div></div></div><div class="uk-width-1-2@m" ><div class="contenedor"><div class="derecha">derecha</div></div></div></div></li>');
  	//$('.uk-slideshow-items').append('hola');
  	var indice = $('.uk-slideshow-items li').length -1;
  	UIkit.slideshow(".uk-slideshow").show(indice);

  },

    borrar_contenido: function () {
      this.resetear_valores();
      
      this.contenido_guardado = "";
      contenido.innerHTML="";   
    },
      presiona_enter_guarda_contenido: function () {
          $("input, textarea").click(function(event) {
          
              alert()
            
          });
      },
      set_id_izquierda: function() {
        $('li.uk-active .izquierda').attr('id','contenido');
        $('li.uk-active .derecha').removeAttr('id','contenido');
      },
      set_id_derecha: function() {
      	$('li.uk-active .derecha').attr('id','contenido');
      	$('li.uk-active .izquirda').removeAttr('id','contenido');
      },

}
})
});

</script>
<!-- <script src="js/jquery-3.3.1.min.js"></script> -->
<!-- <script src="js/uikit.min.js"></script>
<script src="js/uikit-icons.js"></script> -->
<!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->
<!-- <script src="js/vue.js"></script>	
  <script src="js/axios.js"></script>	
  <script src="js/toastr.js"></script> -->	
</body>
</html>

