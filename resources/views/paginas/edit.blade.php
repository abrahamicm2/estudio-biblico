@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Pagina
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($pagina, ['route' => ['paginas.update', $pagina->id], 'method' => 'patch']) !!}

                        @include('paginas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection