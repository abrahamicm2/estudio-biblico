<table class="table table-responsive" id="paginas-table">
    <thead>
        <th>Contenido</th>
        <th>Tema</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($paginas as $pagina)
        <tr>
            <td>{!! $pagina->contenido !!}</td>
            <td>{!! $pagina->temas->nombre !!}</td>
            <td>
                {!! Form::open(['route' => ['paginas.destroy', $pagina->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('paginas.show', [$pagina->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('paginas.edit', [$pagina->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>