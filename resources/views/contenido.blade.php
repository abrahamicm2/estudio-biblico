
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
<title>Learning</title>
<link rel="stylesheet" type="text/css" href="{{asset('css/uikit.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/css2.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/boot.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/book.css')}}">
<style type="text/css">
  a {line-height: 2.4}
  .contenedor{ margin: 20px;	 }
</style>
</head>
<body>
 
<div  class="bg-inverse uk-light fixed-top" uk-grid>
  <div class="uk-width-1-2">  
    <a class="uk-margin-left"><span class="uk-margin-small-right" uk-icon="icon: list; ratio: 1"></span></a>
    <a href="#"><span class="uk-margin-small-right" uk-icon="icon: tag; ratio: 1"></span></a>
    <a href="#"><span class="uk-margin-small-right" uk-icon="icon: search; ratio: 1"></span></a>  
  </div>
  <div class="uk-width-1-2 uk-text-right">
    <a href="#"><span class="uk-margin-small-right" uk-icon="icon:  pencil; ratio: 1"></span></a>     
    <a href="#"><span class="uk-margin-small-right" uk-icon="icon:  play; ratio: 1"></span></a>   
    <a href="#" class="uk-margin-right"><span class="uk-margin-small-right" uk-icon="icon: bookmark; ratio: 1"></span></a>  
  </div>
</div>


<div class="muestra-barra" style="margin-top: 50px;">
  <div class="uk-position-relative" uk-slideshow >
    <ul class="uk-slideshow-items "   uk-height-viewport="expand: true;min-height: 300">
      
        @foreach ($estudio->temas as $tema)
          @foreach ($tema->paginas as $pagina)
            <li style="overflow: auto; padding: 20px 10%">
            {!!$pagina->contenido!!}
            </li>
          @endforeach
        @endforeach
     
 
 

    </ul>
    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
  </div>
</div>

<div class="fixed-bottom uk-text-center">
  <div class="uk-margin uk-width-5-6 uk-align-center">
    <input class="uk-range" type="range" value="1" min="1" max="10" step="0.1"  id="rango">
  </div>
  <div  class="bg-inverse uk-light" uk-grid>
    <div class="uk-width-1-3">Titulo del estudio</div>
    <div class="uk-width-1-3" id="porcentaje"></div>
    <div class="uk-width-1-3">Capitulo</div>
  </div>
</div>
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/uikit.min.js') }}"></script>
<script src="{{ asset('js/uikit-icons.js') }}"></script>
<!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->
<!-- <script src="js/vue.js"></script>	
  <script src="js/axios.js"></script>	
  <script src="js/toastr.js"></script> -->	

  <script type="text/javascript">
   

$(document).ready(function() {
  $(".fixed-bottom , .bg-inverse").hide();
  $(".contenedor").click(function(){
    $(".fixed-bottom , .bg-inverse ").fadeToggle()
  })

 var rango =document.getElementById("rango")
 var div_porcentaje =document.getElementById("porcentaje");
 
 var medida= $('.uk-slideshow-items li').length -1;

  var divisor= 10/medida;
 // var = indice_medida;
 // rango.step= divisor;
 rango.value=0;

 rango.addEventListener("change",function(){
   div_porcentaje.innerText = Math.floor(rango.value* divisor*medida)  + "%";

   //console.log(rango.value/divisor)

   UIkit.slideshow(".uk-slideshow").show( Math.round(rango.value/divisor));
 });

 UIkit.util.on('.uk-slideshow', 'itemshow', function () {
    rango.value= divisor*UIkit.slideshow(".uk-slideshow").index;
   // alert(divisor*medida)
      div_porcentaje.innerText = Math.floor(rango.value* divisor*medida)  + "%";

});

 $(document).keyup(function(event) {
   /* Act on the event */
   if (event.keyCode==39) {
    //UIkit.slideshow(".uk-slideshow").index;
    if (UIkit.slideshow(".uk-slideshow").index<medida) {
      UIkit.slideshow(".uk-slideshow").show(UIkit.slideshow(".uk-slideshow").index+1);
    }
   // else {UIkit.slideshow(".uk-slideshow").show(0);}
   }
   else if (event.keyCode==37) {
    if (UIkit.slideshow(".uk-slideshow").index>0) {
      UIkit.slideshow(".uk-slideshow").show(UIkit.slideshow(".uk-slideshow").index-1);
    }
   // else {UIkit.slideshow(".uk-slideshow").show(0);}

   }
 });
  
});
     
      //UIkit.slideshow(".uk-slideshow").show(indice);
      
   
  </script>
</body>
</html>

