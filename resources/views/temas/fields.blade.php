<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!-- Estudios Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('estudios_id', 'Estudios Id:') !!}
    {!! Form::select('estudios_id',  $estudios, null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('temas.index') !!}" class="btn btn-default">Cancel</a>
</div>
