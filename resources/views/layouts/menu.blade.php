<li class="{{ Request::is('estudios*') ? 'active' : '' }}">
    <a href="{!! route('estudios.index') !!}"><i class="fa fa-edit"></i><span>Estudios</span></a>
</li>

<li class="{{ Request::is('temas*') ? 'active' : '' }}">
    <a href="{!! route('temas.index') !!}"><i class="fa fa-edit"></i><span>Temas</span></a>
</li>

<li class="{{ Request::is('paginas*') ? 'active' : '' }}">
    <a href="{!! route('paginas.index') !!}"><i class="fa fa-edit"></i><span>Paginas</span></a>
</li>

{{-- <li class="{{ Request::is('imagens*') ? 'active' : '' }}">
    <a href="{!! route('imagens.index') !!}"><i class="fa fa-edit"></i><span>imagens</span></a>
</li>
 --}}
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

