@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Imagen
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($imagen, ['route' => ['imagens.update', $imagen->id], 'method' => 'patch']) !!}

                        @include('imagens.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection