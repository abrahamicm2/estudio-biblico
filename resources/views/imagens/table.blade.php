<table class="table table-responsive" id="imagens-table">
    <thead>
        <th>Imagen</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($imagens as $imagen)
        <tr>
            <td>{!! $imagen->imagen !!}</td>
            <td>
                {!! Form::open(['route' => ['imagens.destroy', $imagen->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('imagens.show', [$imagen->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('imagens.edit', [$imagen->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>