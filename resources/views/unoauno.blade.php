
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
<title>Learning</title>
<link rel="stylesheet" type="text/css" href="{{asset('css/uikit.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/css2.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/boot.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('css/book.css')}}">
<style type="text/css">
  a {line-height: 2.4}
  .contenedor{ margin: 20px;	 }
</style>
</head>
<body>
 
<div  class="bg-inverse uk-light fixed-top" uk-grid>
  <div class="uk-width-1-2">  
    <a href="/home" class="uk-margin-left"><span class="uk-margin-small-right" uk-icon="icon: list; ratio: 1"></span></a>
    <a href="#"><span class="uk-margin-small-right" uk-icon="icon: tag; ratio: 1"></span></a>
    <a href="#"><span class="uk-margin-small-right" uk-icon="icon: search; ratio: 1"></span></a>  
  </div>
  <div class="uk-width-1-2 uk-text-right">
    <a href="#"><span class="uk-margin-small-right" uk-icon="icon:  pencil; ratio: 1"></span></a>     
    <a href="#"><span class="uk-margin-small-right" uk-icon="icon:  play; ratio: 1"></span></a>   
    <a href="#" class="uk-margin-right"><span class="uk-margin-small-right" uk-icon="icon: bookmark; ratio: 1"></span></a>  
  </div>
</div>


<div class="muestra-barra" style="margin-top: 50px;">
  <div class="uk-position-relative" uk-slideshow >
    <ul class="uk-slideshow-items "   uk-height-viewport="expand: true;min-height: 300">
      <li style="overflow: auto;">
        <div class="uk-margin" uk-grid>
          <div class="uk-width-1-2@m">
            <div class="contenedor">
              <p class="s1 s2"> </p>
              <p class="s1 s2"> </p>
              <p class="s1 s2">Uno a  Uno</p>
              <p class="s3">EPUB, <span class="c1">Primera Edición</span> </p>
              <p class="s4"> </p>
              <p class="s1"> </p>
              <h1 class="s5"> </h1>
            </div>
          </div>
          <div class="uk-width-1-2@m">
            <div class="contenedor">
              <h3 class="s6">1) ¿Tengo Vida Eterna?</h3>
              <p class="s7"> </p>
              <p class="s8 s9">Memoriza</p>
              <p class="s10">&quot;Más a todos los que le recibieron, a los que creen en su nombre, les dio potestad de ser hechos hilos de Dios”<br/>Juan 1:12</p>
              <p class="s8 s9">Reflexión</p>
              <p class="s7"> </p>
              <p class="s7">¿Cuánto tiempo gastaríamos contando la arena de una playa? Aunque parezca imposible, su número es finito, se puede contar. Dios, el Creador, es infinito, ha existido desde siempre, existe y existirá para siempre. ¿Cuánto tiempo gastarías en conocerlo?. La respuesta es obvia. ¡Una eternidad!!</p>
              <p class="s8 s9">1. La vida eterna</p>
              <p class="s10">&quot;Y esta es la vida eterna: que te conozcan a ti el único Dios verdadero, ya Jesucristo, a quien has enviado”.<br/>Juan 17:3</p>
              <p class="s7">¿Qué es la vida eterna?<br/>¿Quién es el enviado del Dios verdadero?</p>
            </div>
          </div>
        </div>
        <br><br><br><br>
      </li>

      <li style="overflow: auto;">
        <div class="uk-margin" uk-grid>
          <div class="uk-width-1-2@m">
            <div class="contenedor">
              <p class="s10">&quot;Y esta es la condenación: que la luz vino al mundo y los hombres amaron más las tinieblas que la luz, porque sus obras eran malas. Porque todo aquel que hace lo malo, aborrece la luz y no viene a la luz, para que sus obras no sean reprendidas. Mas el que practica la verdad viene a la luz, para que sea manifiesto que sus obras son hechas en Dios&quot;.<br/> Juan 3:19-21</p>
              <p class="s7">¿Cuál es la condenación eterna?<br/>¿Por qué los hombres prefieren las tinieblas?</p>
              <p class="s10">&quot;Otra vez Jesús les habló, diciendo: Yo soy la luz del mundo; el que me sigue, no andará en tinieblas, sino que tendrá la luz de la vida&quot;.<br/> Juan 8:12</p>
              <p class="s7">¿Quién es la luz del mundo?<br/>¿Qué le sucede al que sigue a Jesús?</p>
              <p class="s8 s9">2. ¿Cómo obtuviste la vida eterna?</p>
              <p class="s7"> </p>
            </div>
          </div>
          <div class="uk-width-1-2@m">
            <div class="contenedor">
              <p class="s10">&quot;Así que la fe es por el oír, y el oír, por la palabra de Dios&quot;.<br/>Romanos 10:17</p>
              <p class="s7">Nuestra fe proviene de haber oído y recibido_____</p>
              <p class="s10">&quot;En él también vosotros, habiendo oído la palabra de verdad, el evangelio de vuestra salvación, y habiendo creído en él, fuisteis sellados con el Espíritu Santo de la promesa, que es las arras de nuestra herencia hasta la redención de la posesión adquirida, para alabanza de su gloria”.<br/> Efesios 1:13-14</p>
              <p class="s7">¿Cuál es la palabra de verdad?<br/>¿Qué condiciones debemos cumplir para ser sellados con el<br/>Espíritu de Dios? 1) _________ 2)________</p>
              <p class="s10">&quot;Porque por gracia sois salvos por medio de la fe; y esto no de vosotros, pues es don de Dios; no por obras, para que nadie se gloríe. Porque somos hechura suya, creados en Cristo Jesús para buenas obras, las cuales Dios preparó de antemano para que anduviésemos en ellas”.<br/>Efesios 2:8-10</p>
             
            </div>
          </div>
        </div>
        <br><br>
      </li>
 <!-- 3 -->

 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
   <p class="s7">¿Qué hace Dios en su gracia?<br/>¿Nos pueden salvar nuestras buenas obras?</p><p class="s10">&quot;Que si confesares con tu boca que Jesús es el Señor, y, creyeres en tu corazón que Dios le levantó de los muertos serás salvo. Porque con el corazón se cree para justicia, pero con la boca se confiesa para salvación”.<br/> Romanos 10:9-10</p><p class="s7">¿Qué debemos recibir para ser salvos?<br/>Para ser salvo, ¿Qué harías tú? Marca tu respuesta:<br/>1) Confesar con tu boca que Jesús es el Señor.<br/>2) Creer en tu corazón que Dios lo levantó de los muertos.</p><p class="s8 s9">3. Yo tengo vida eterna</p><p class="s10">&quot;Le dijo Jesús: Yo soy la resurrección y la vida; el que cree en mí, aunque esté muerto, vivirá. Y todo aquel que vive y cree en mí, no morirá eternamente. ¿Crees esto? Le dijo: Sí, Señor; yo he creído que tú eres el Cristo, el Hijo de Dios, que has venido al mundo”.<br/> Juan 11:25-27</p>
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
         <p class="s7">¿Quién es Jesús?<br/>¿Qué nos prometió el Hijo de Dios?<br/>¿Qué responderías tú, si Cristo te hiciera la pregunta del versículo 26,<br/>¿crees esto?</p><p class="s10">&quot;De cierto, de cierto os digo: El que oye mi palabra, y cree al que me envió, tiene vida eterna; y no vendrá a condenación, mas ha pasado de muerte a vida&quot;.<br/>  Juan 5:24</p><p class="s7">Hay tres promesas que se cumplen en el momento de oír y creer en su Palabra. <br/>Escríbelas:</p><p class="s10">&quot;Y este es el testimonio: que Dios nos ha dado vida eterna; y esta vida está en su Hijo. El que tiene al Hijo, tiene la vida; el que no tiene al Hijo de Dios no tiene la vida. Estas cosas os he escrito a vosotros que creéis en el nombre del Hijo de Dios, para que sepáis que tenéis vida eterna, y para que creáis en el nombre del Hijo de Dios”.<br/> 1 Juan 5:11-13</p>
        
       </div>
     </div>
   </div>
   <br><br>
 </li>
 <!-- 4 -->

 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      <p class="s7">¿Quién nos dio vida eterna?<br/>¿Quién es la vida eterna?<br/>¿Desde cuándo tienes vida eterna?<br/>¿A quién debes tener para ser salvo?</p><p class="s10">&quot;Todo lo que el Padre me da, vendrá a mi; y al que a mí viene, no le echo fuera&quot;.<br/> Juan 6:37</p><p class="s7">¿Podemos perder la vida que recibimos?<br/>¿Quién nos la asegura?</p><p class="s10">&quot;Y esta es la voluntad del padre, el que me envió: Que de todo lo que me diere, no pierda yo nada, sino que lo resucite en el día postrero”.<br/> Juan 6:39</p><p class="s7">¿Quién garantiza mi resurrección?</p><p class="s8 s9">4. Mi responsabilidad</p>
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     <p class="s10">&quot;Vosotros también, poniendo toda diligencia por esto mismo, añadid a vuestra fe virtud; a la virtud, conocimiento; al conocimiento, dominio propio; al dominio propio, paciencia; a la paciencia, piedad; a la piedad, afecto fraternal; y al afecto fraternal, amor. Porque si estas cosas están en vosotros, y abundan, no os dejarán estar ociosos ni sin fruto en cuanto al conocimiento de nuestro Señor Jesucristo&quot;.<br/> 2 Pedro 1:5-8</p><p class="s7">¿Qué características deben abundar en nosotros, para no estar ociosos ni sin fruto?<br/>¿Qué ganamos al tener estas cosas?<br/>¿Quiénes deben poner empeño en tenerlas?</p><p class="s7">Una de nuestras prioridades debe ser la de añadir estas características a nuestra vida. Nuestra disposición con el poder de Dios resultará en nuestra madurez espiritual.</p>
     <p class="s7">Ahora veremos cómo añadir estas características en una forma</p><p class="s7">práctica.</p><p class="s10">&quot;Sino que en la ley de Jehová está su delicia, Y en su ley medita de día y de noche&quot;.<br/> Salmo 1:2</p>
        
       </div>
     </div>
   </div>
   <br><br>
 </li>
 <!-- 5 -->

 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     <p class="s7">¿En qué debemos meditar constantemente?<br/>¿Qué significa “meditar&quot;?</p><p class="s10">&quot;Orad sin cesar&quot;. 1 Tesalonicenses 5:17</p><p class="s7">¿Qué debemos hacer constantemente?<br/>¿Es un mandato o una sugerencia?</p><p class="s10">&quot;Por nada estéis afanosos, sino sean conocidas vuestras peticiones delante de Dios en toda oración y ruego, con acción de gracias. Y la paz de Dios, que sobrepasa todo entendimiento, guardará vuestros corazones y vuestros pensamientos en Cristo Jesús”.<br/> Filipenses 4:6-7</p><p class="s7">¿Qué significa orar?</p><p class="s10">&quot;Si me amáis, guardad mis mandamientos”. <br/> Juan 14:15</p><p class="s7">¿Qué hacemos para demostrar nuestro amor a Dios?</p>
     <p class="s10">&quot;Y considerémonos unos a otros para estimularnos al amor y a las buenas obras&quot;.  Hebreos 10:24</p>
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      <p class="s7">¿Cuál es nuestra responsabilidad ante nuestros hermanos en Cristo?</p><p class="s10">&quot;No dejando de congregarnos, como algunos tienen por costumbre, sino exhortándonos; y tanto más, cuanto veis que aquel día se acerca”.<br/> Hebreos 10:25</p><p class="s7">¿Qué debemos hacer constantemente?</p><p class="s8 s9">5. Memoriza Juan 1:12</p><p class="s7">Ejercita tu memoria escribiendo este versículo sin mirar la Palabra de Dios.</p><p class="s8 s9">Sabías tú que...</p><p class="s7">Aunque ya eres salvo, hay algo que pide el Señor a cada creyente. Un paso de obediencia para demostrar tu fe en el Señor Jesucristo, es dar testimonio público de tu cambio de vida. En la próxima lección aprenderás qué significa el bautismo.</p>
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
 <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
  <!-- 6 -->
 <li style="overflow: auto;">
   <div class="uk-margin" uk-grid>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
     
       </div>
     </div>
     <div class="uk-width-1-2@m">
       <div class="contenedor">
      
       </div>
     </div>
   </div>
   <br><br>
 </li>
 
 

    </ul>
    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>
  </div>
</div>

<div class="fixed-bottom uk-text-center">
  <div class="uk-margin uk-width-5-6 uk-align-center">
    <input class="uk-range" type="range" value="1" min="1" max="10" step="0.1"  id="rango">
  </div>
  <div  class="bg-inverse uk-light" uk-grid>
    <div class="uk-width-1-3">Titulo del estudio</div>
    <div class="uk-width-1-3" id="porcentaje"></div>
    <div class="uk-width-1-3">Capitulo</div>
  </div>
</div>
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/uikit.min.js') }}"></script>
<script src="{{ asset('js/uikit-icons.js') }}"></script>
<!-- <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script> -->
<!-- <script src="js/vue.js"></script>	
  <script src="js/axios.js"></script>	
  <script src="js/toastr.js"></script> -->	

  <script type="text/javascript">
   

$(document).ready(function() {
  $(".fixed-bottom , .bg-inverse").hide();
  $(".contenedor").click(function(){
    $(".fixed-bottom , .bg-inverse ").fadeToggle()
  })

 var rango =document.getElementById("rango")
 var div_porcentaje =document.getElementById("porcentaje");
 
 var medida= $('.uk-slideshow-items li').length -1;

  var divisor= 10/medida;
 // var = indice_medida;
 // rango.step= divisor;
 rango.value=0;

 rango.addEventListener("change",function(){
   div_porcentaje.innerText = Math.floor(rango.value* divisor*medida)  + "%";

   //console.log(rango.value/divisor)

   UIkit.slideshow(".uk-slideshow").show( Math.round(rango.value/divisor));
 });

 UIkit.util.on('.uk-slideshow', 'itemshow', function () {
    rango.value= divisor*UIkit.slideshow(".uk-slideshow").index;
   // alert(divisor*medida)
      div_porcentaje.innerText = Math.floor(rango.value* divisor*medida)  + "%";

});

 $(document).keyup(function(event) {
   /* Act on the event */
   if (event.keyCode==39) {
    //UIkit.slideshow(".uk-slideshow").index;
    if (UIkit.slideshow(".uk-slideshow").index<medida) {
      UIkit.slideshow(".uk-slideshow").show(UIkit.slideshow(".uk-slideshow").index+1);
    }
   // else {UIkit.slideshow(".uk-slideshow").show(0);}
   }
   else if (event.keyCode==37) {
    if (UIkit.slideshow(".uk-slideshow").index>0) {
      UIkit.slideshow(".uk-slideshow").show(UIkit.slideshow(".uk-slideshow").index-1);
    }
   // else {UIkit.slideshow(".uk-slideshow").show(0);}

   }
 });
  
});
     
      //UIkit.slideshow(".uk-slideshow").show(indice);
      
   
  </script>
</body>
</html>

