<!DOCTYPE html>
<html lang="en">
<head>
  <title>Estudio Biblico</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!--  <script src="https://unpkg.com/vue"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
  <link rel="stylesheet" type="text/css" href="css/css.css">
  <style type="text/css">
    body {
        background-color: #fff;
        color: block;
    }
    a , a:hover
    {
        color: #000;
        text-decoration: none ;
    }
  </style>
</head>
<body>
    <div>
        
<img src="img/cabecera.png" style="width: 100%">  
    </div>
  <div class="container">
    <div class="row mt20">
        <div class="col-sm-4">
            <form method="POST" action="{{url('logout')}}">
            <a href="{{route('admin')}}" class=" btn btn-link" style="text-decoration: none !important">administrar</a>
            <button type="submit" class="btn btn-link" style="text-decoration: none !important">cerrar sesion</button>
            {{ csrf_field() }}

            </form>
  
  </div>
        <div class="col-sm-2 pull-right"><input type="" name=""></div>
    </div>

@php
    $contador=0;
    $contador2=1;
@endphp

@foreach ($estudios as $estudio)
@php $contador++;
$contador2++;
 @endphp
@if ($contador2 %2==0)
    <div class="row mt10">
@endif
@if ($contador % 2 != 0)
  

 <div class="col-md-6">
     <img src="/~impactoresources/storage/app/{{$estudio->imagen}}" class="img-responsive" style="width: 241px;height: 350px; margin: 0 auto">
     <h3 style="text-align: center;">{{$estudio->nombre}}</h3>
     <p style="width: 241px;text-align: justify;margin: 0 auto">
       {{$estudio->descripcion}}<br><br>
        <a href="contenido/{{$estudio->id}}" class="btn btn-primary">Empezar</a> 
     </p>
    
 </div> 
@elseif($contador % 2 == 0)
    <div class="col-md-6">
        <img src="/~impactoresources/storage/app/{{$estudio->imagen}}" class="img-responsive" style="width: 241px;height: 350px; margin: 0 auto">
        <h3 style="text-align: center;">{{$estudio->nombre}}</h3>
        <p style="width: 241px;text-align: justify;margin: 0 auto">
          {{$estudio->descripcion}}<br><br>
            
        <a href="contenido/{{$estudio->id}}" class="btn btn-primary">Empezar</a>
        </p>
    </div>
    </div>

@endif

   
@endforeach


  </div>
  <br>
  <footer class="row footer-estudio">
    <div class="col-md-9">2018 © Copyright., IMPACTO | info@impactohn.org</div>
    <div class="col-md-3">SOPORTE</div>
  </footer>
</body>
</html>