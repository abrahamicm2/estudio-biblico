<?php

namespace App\Repositories;

use App\Models\Pagina;
use InfyOm\Generator\Common\BaseRepository;

class PaginaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'contenido',
        'tema_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pagina::class;
    }
}
