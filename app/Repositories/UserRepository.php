<?php

namespace App\Repositories;

use App\Models\User;
use InfyOm\Generator\Common\BaseRepository;

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'apellido',
        'cumpleanos',
        'iglesia',
        'avatar',
        'email',
        'password',
        'telefono',
        'remember_token',
        'active',
        'social',
        'type'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
}
