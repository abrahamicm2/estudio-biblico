<?php

namespace App\Repositories;

use App\Models\imagen;
use InfyOm\Generator\Common\BaseRepository;

class imagenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'imagen'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return imagen::class;
    }
}
