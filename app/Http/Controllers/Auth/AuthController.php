<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Mail;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => 'required|string|max:255',
    //         'email' => 'required|string|email|max:255|unique:users',
    //         'password' => 'required|string|min:6|confirmed',
    //     ]);
    // }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(Request $request)
    {

          $user = new User;
          $data['name'] = $user->name = $request->name;
          $data['email'] = $user->email = $request->email;
          $data['confirm_token'] = $user->confirm_token = str_random(100);

          $data['apellido'] = $user->apellido = $request->apellido;
          $data['cumpleanos'] = $user->cumpleanos = $request->cumpleanos;
          $data['iglesia'] = $user->iglesia = $request->iglesia;
          $data['avatar'] = $user->avatar = $request->avatar;
          $data['telefono'] = $user->telefono = $request->telefono;

          $user->password = bcrypt($request->password);
          $user->remember_token = str_random(100);
          $user->save();
          
          Mail::send('mails.register', ['data' => $data], function($mail) use($data){
              $mail->subject('Confirma tu cuenta');
              $mail->to($data['email'], $data['name']);
          });
          
          return redirect("/home")->with("message", "Hemos enviado un enlace de confirmación a su cuenta de correo electrónico");
               
          //return dd("hola");

    }       
    
    public function confirmRegister($email, $confirm_token)
    {
      $user = new User;
      $the_user = $user->select()->where('email', '=', $email)
      ->where('confirm_token', '=', $confirm_token)->get();
     
      if (count($the_user) > 0)
      {
        $active = 1;
        $confirm_token = str_random(100);
        $user->where('email', '=', $email)
        ->update(['active' => $active, 'confirm_token' => $confirm_token]);
        return redirect('/home')->with('message', 'Enhorabuena ' . $the_user[0]['name'] . ' ya puede iniciar sesión');
      }
      else
      {
        return redirect('/');
      }
    }
}