<?php

namespace App\Http\Controllers;
use App\Models\Estudio;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $estudio= Estudio::findOrFail(3);
        $estudios= Estudio::all();
        return view('indice',compact('estudios'));
    }
}
