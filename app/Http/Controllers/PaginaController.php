<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePaginaRequest;
use App\Http\Requests\UpdatePaginaRequest;
use App\Repositories\PaginaRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Tema;

class PaginaController extends AppBaseController
{
    /** @var  PaginaRepository */
    private $paginaRepository;

    public function __construct(PaginaRepository $paginaRepo)
    {
        $this->paginaRepository = $paginaRepo;
    }

    /**
     * Display a listing of the Pagina.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->paginaRepository->pushCriteria(new RequestCriteria($request));
        $paginas = $this->paginaRepository->all();

        return view('paginas.index')
            ->with('paginas', $paginas);
    }

    /**
     * Show the form for creating a new Pagina.
     *
     * @return Response
     */
    public function create()
    {
        $temas= Tema::pluck('nombre','id');
        return view('paginas.create', compact("temas"));
    }

    /**
     * Store a newly created Pagina in storage.
     *
     * @param CreatePaginaRequest $request
     *
     * @return Response
     */
    public function store(CreatePaginaRequest $request)
    {
        $input = $request->all();

        $pagina = $this->paginaRepository->create($input);

        Flash::success('Pagina saved successfully.');

        return redirect(route('paginas.index'));
    }

    /**
     * Display the specified Pagina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $temas= Tema::pluck('nombre','id');
        $pagina = $this->paginaRepository->findWithoutFail($id);

        if (empty($pagina)) {
            Flash::error('Pagina not found');

            return redirect(route('paginas.index'));
        }

        return view('paginas.show')->with('pagina', $pagina)->with('temas',$temas);
    }

    /**
     * Show the form for editing the specified Pagina.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
   
        $pagina = $this->paginaRepository->findWithoutFail($id);

        $temas= Tema::pluck('nombre','id');

        if (empty($pagina)) {
            Flash::error('Pagina not found');

            return redirect(route('paginas.index'));
        }

        return view('paginas.edit')->with('pagina', $pagina)->with('temas',$temas);
    }

    /**
     * Update the specified Pagina in storage.
     *
     * @param  int              $id
     * @param UpdatePaginaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaginaRequest $request)
    {
        $pagina = $this->paginaRepository->findWithoutFail($id);

        if (empty($pagina)) {
            Flash::error('Pagina not found');

            return redirect(route('paginas.index'));
        }

        $pagina = $this->paginaRepository->update($request->all(), $id);

        Flash::success('Pagina updated successfully.');

        return redirect(route('paginas.index'));
    }

    /**
     * Remove the specified Pagina from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $pagina = $this->paginaRepository->findWithoutFail($id);

        if (empty($pagina)) {
            Flash::error('Pagina not found');

            return redirect(route('paginas.index'));
        }

        $this->paginaRepository->delete($id);

        Flash::success('Pagina deleted successfully.');

        return redirect(route('paginas.index'));
    }
}
