<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateimagenRequest;
use App\Http\Requests\UpdateimagenRequest;
use App\Repositories\imagenRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class imagenController extends AppBaseController
{
    /** @var  imagenRepository */
    private $imagenRepository;

    public function __construct(imagenRepository $imagenRepo)
    {
        $this->imagenRepository = $imagenRepo;
    }

    /**
     * Display a listing of the imagen.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->imagenRepository->pushCriteria(new RequestCriteria($request));
        $imagens = $this->imagenRepository->all();

        return view('imagens.index')
            ->with('imagens', $imagens);
    }

    /**
     * Show the form for creating a new imagen.
     *
     * @return Response
     */
    public function create()
    {
        return view('imagens.create');
    }

    /**
     * Store a newly created imagen in storage.
     *
     * @param CreateimagenRequest $request
     *
     * @return Response
     */
    public function store(CreateimagenRequest $request)
    {
        $input = $request->all();

        $imagen = $this->imagenRepository->create($input);

        Flash::success('Imagen saved successfully.');

        return redirect(route('imagens.index'));
    }

    /**
     * Display the specified imagen.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $imagen = $this->imagenRepository->findWithoutFail($id);

        if (empty($imagen)) {
            Flash::error('Imagen not found');

            return redirect(route('imagens.index'));
        }

        return view('imagens.show')->with('imagen', $imagen);
    }

    /**
     * Show the form for editing the specified imagen.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $imagen = $this->imagenRepository->findWithoutFail($id);

        if (empty($imagen)) {
            Flash::error('Imagen not found');

            return redirect(route('imagens.index'));
        }

        return view('imagens.edit')->with('imagen', $imagen);
    }

    /**
     * Update the specified imagen in storage.
     *
     * @param  int              $id
     * @param UpdateimagenRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateimagenRequest $request)
    {
        $imagen = $this->imagenRepository->findWithoutFail($id);

        if (empty($imagen)) {
            Flash::error('Imagen not found');

            return redirect(route('imagens.index'));
        }

        $imagen = $this->imagenRepository->update($request->all(), $id);

        Flash::success('Imagen updated successfully.');

        return redirect(route('imagens.index'));
    }

    /**
     * Remove the specified imagen from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $imagen = $this->imagenRepository->findWithoutFail($id);

        if (empty($imagen)) {
            Flash::error('Imagen not found');

            return redirect(route('imagens.index'));
        }

        $this->imagenRepository->delete($id);

        Flash::success('Imagen deleted successfully.');

        return redirect(route('imagens.index'));
    }
}
