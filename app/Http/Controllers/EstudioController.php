<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEstudioRequest;
use App\Http\Requests\UpdateEstudioRequest;
use App\Repositories\EstudioRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\Models\Tema;


class EstudioController extends AppBaseController
{
    /** @var  EstudioRepository */
    private $estudioRepository;

    public function __construct(EstudioRepository $estudioRepo)
    {
        $this->estudioRepository = $estudioRepo;
    }

    /**
     * Display a listing of the Estudio.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->estudioRepository->pushCriteria(new RequestCriteria($request));
        $estudios = $this->estudioRepository->all();

     

        return view('estudios.index')
            ->with('estudios', $estudios);
    }

    /**
     * Show the form for creating a new Estudio.
     *
     * @return Response
     */
    public function create()
    {
        $temas = Tema::all();
        return view('estudios.create');
    }

    /**
     * Store a newly created Estudio in storage.
     *
     * @param CreateEstudioRequest $request
     *
     * @return Response
     */
    public function store(CreateEstudioRequest $request)
    {
        $input = [
         'nombre' => $request->nombre,
         'categoria' => $request->categoria,
         'descripcion' => $request->descripcion,
         'url' => $request->url,
         'imagen' => $request->file('imagen')->store('public/img')
        ];
        
         
        

        $estudio = $this->estudioRepository->create($input);
           
       

        Flash::success('Estudio saved successfully.');

        return redirect(route('estudios.index'));
    }

    /**
     * Display the specified Estudio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $estudio = $this->estudioRepository->findWithoutFail($id);

        if (empty($estudio)) {
            Flash::error('Estudio not found');

            return redirect(route('estudios.index'));
        }

        return view('estudios.show')->with('estudio', $estudio);
    }

    /**
     * Show the form for editing the specified Estudio.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $estudio = $this->estudioRepository->findWithoutFail($id);

        if (empty($estudio)) {
            Flash::error('Estudio not found');

            return redirect(route('estudios.index'));
        }

        return view('estudios.edit')->with('estudio', $estudio);
    }

    /**
     * Update the specified Estudio in storage.
     *
     * @param  int              $id
     * @param UpdateEstudioRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEstudioRequest $request)
    {
       //dd($request->file('imagen'));
        $estudio = $this->estudioRepository->findWithoutFail($id);
        $input = [
         'nombre' => $request->nombre,
         'categoria' => $request->categoria,
         'descripcion' => $request->descripcion,
         'url' => $request->url
        //'imagen' => $request->file('imagen')->store('public/img')
        ];
            
        if ($request->hasFile('imagen')){
          $input['imagen'] =  $request->file('imagen')->store('public/img');
            }


        if (empty($estudio)) {
            Flash::error('Estudio not found');

            return redirect(route('estudios.index'));
        }

        $estudio = $this->estudioRepository->update($input, $id);

        Flash::success('Estudio updated successfully.');

        return redirect(route('estudios.index'));
    }

    /**
     * Remove the specified Estudio from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $estudio = $this->estudioRepository->findWithoutFail($id);

        if (empty($estudio)) {
            Flash::error('Estudio not found');

            return redirect(route('estudios.index'));
        }

        $this->estudioRepository->delete($id);

        Flash::success('Estudio deleted successfully.');

        return redirect(route('estudios.index'));
    }
    
    public function mostrar($id)
    {
        $estudio = $this->estudioRepository->findWithoutFail($id);


        return view('contenido')->with('estudio', $estudio);
    }


}
