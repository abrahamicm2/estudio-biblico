<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 * @package App\Models
 * @version February 20, 2018, 1:12 pm UTC
 */
class User extends Model
{
    use SoftDeletes;

    public $table = 'users';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'apellido',
        'cumpleanos',
        'iglesia',
        'avatar',
        'email',
        'password',
        'telefono',
        'remember_token',
        'active',
        'social',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'apellido' => 'string',
        'cumpleanos' => 'string',
        'iglesia' => 'string',
        'avatar' => 'string',
        'email' => 'string',
        'password' => 'string',
        'telefono' => 'string',
        'remember_token' => 'string',
        'active' => 'boolean',
        'social' => 'boolean',
        'type' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
