<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pagina
 * @package App\Models
 * @version February 15, 2018, 6:57 am UTC
 */
class Pagina extends Model
{
    use SoftDeletes;

    public $table = 'paginas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'contenido',
        'tema_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'contenido' => 'string',
        'tema_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    public function temas()
    {
        # code...
        return $this->belongsTo('App\Models\Tema','tema_id');
    }
    
}
