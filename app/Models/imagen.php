<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class imagen
 * @package App\Models
 * @version February 19, 2018, 4:07 pm UTC
 */
class imagen extends Model
{
    use SoftDeletes;

    public $table = 'imagens';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'imagen'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'imagen' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
