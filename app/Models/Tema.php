<?php
namespace App\Models;
use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class Tema
 * @package App\Models
 * @version February 15, 2018, 6:22 am UTC
 */
class Tema extends Model
{
    use SoftDeletes;
    public $table = 'temas';   
    protected $dates = ['deleted_at'];
    public $fillable = [
        'nombre',
        'estudios_id'
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string',
        'estudios_id' => 'integer'
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
    public function estudio()
    {
        # code...
        return $this->belongsTo('App\Models\Estudio','estudios_id');
        
    }

    public function paginas()
{
    # code...
    return $this->hasMany('App\Models\Pagina','tema_id');
}

}
