<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Estudio
 * @package App\Models
 * @version February 15, 2018, 6:17 am UTC
 */
class Estudio extends Model
{
    use SoftDeletes;

    public $table = 'estudios';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nombre',
        'categoria',
        'descripcion',
        'url',
        'imagen'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nombre' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];
public function temas()
{
    # code...
    return $this->hasMany('App\Models\Tema','estudios_id');
}
    
}
