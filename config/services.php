<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    
    'facebook' => [
     'client_id' => '2351926108166280',
     'client_secret' => 'd84af738118a6b2c262693b6cd8e5395',
     'redirect' => 'http://impactoresources.org/social/callback/facebook',
    ],
    
    'google' => [
     'client_id' => '842251320066-bsrajqemmrl3k9iq8jls73a4ulk5pak1.apps.googleusercontent.com',
     'client_secret' => 'N1KsdS3WnE0TceLdDq98hqeg',
     'redirect' => 'http://impactoresources.org/social/callback/google',
    ],
    'twitter' => [
     'client_id' => 'RKH4NhzNv8QPxh4GmKn85ESJy',
     'client_secret' => 'nGNnj9CX9yr9wMRMxmg5kCNN0hKEZJ5wbdSvJZN4SEWfOC9gKH',
     'redirect' => 'http://impactoresources.org/social/callback/twitter',
    ],

];
